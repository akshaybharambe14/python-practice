# python-practice

Python practice for The Complete Python Masterclass: Learn Python From Scratch.
All examples will be included.
Course link - https://www.udemy.com/python-masterclass-course/

Some other useful resources
1. https://www.w3schools.com/python/
2. https://docs.python.org/3/tutorial/
3. https://www.guru99.com/python-tutorials.html
4. https://www.learnpython.org/
5. https://www.python-course.eu/python3_course.php