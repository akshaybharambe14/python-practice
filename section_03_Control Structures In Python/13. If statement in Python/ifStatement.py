# do a particular task depending on the specified condition

# if true:
#   do this
# else:
# do that

# Todo: check if an individual is an adult or not

age = int(input("Enter your age: "))

if age >= 18:
    print("You are an adult")
else:
    print("You are not an adult")
