# create an number list from 5 - 9
numbers = [5, 6, 7, 8, 9]
# or we can do this,
newNumbers = list(range(5, 10))
print('newNumbers: ', newNumbers)

# also we can specify interval
numbersWithInterval = list(range(0, 50, 10))
print('numbersWithInterval: ', numbersWithInterval)
