# list is like an array which can store multiple values of same or different data type

# create an list of strings with elements
names = ['a', 'b', 'c', 'd']
# print list
print('names = ', names)
# print specific element at particular position starting from 0
print('element at 0 :', names[0])
# what if position is not availbale
# print('element at 0 :', names[9])  # this will throw an error

# create an list of int with elements
numbers = [1, 2, 3, 4, 5]
print('numbers: ', numbers)

# print list with interval
print('numbers from 0 to 2', numbers[0:2])

# create an list with strings and numbers
namesWithNumbers = [1, '1', 2, '2', 3, '3']
print('namesWithNumbers: ', namesWithNumbers)

# create an empty list to be used later
a = []
print('An empty list looks like: ', a)
