# for loop is used to repeat a set of code for specified iterations

# print numbers for 1 to 10

for x in range(1, 11):
    print(x)

# prints all items from list, each element on new line
names = ['a', 'b', 'c']
for x in names:
    print(x)

# break if specific condition is true
for x in range(20):  # this will print till 10
    print(x)
    if x == 10:
        break


def checkEven(a):  # check if given number is even
    if a % 2 == 0:
        return True


# continue to next iteration if specific condition is true
print('This will print all odd numbers')
for x in range(20):
    if checkEven(x) == True:
        continue
    print(x)

# for loop with else
for x in range(10):
    if x == 0:
        print('This will print numbers from 1 to 10 with a message at last')
    print(x)
else:
    print('List ended')  # print after finishing for loop
