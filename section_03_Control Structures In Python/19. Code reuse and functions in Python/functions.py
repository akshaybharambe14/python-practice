# functions help to reuse te repetitive code.
# function definition


def myFunction():
    print("This is your first function")
    print("This code can be reused")


# now call the function
myFunction()

# # function call, this will throw error, because function call should be after function definition
# testFunc()


# def testFunc():  # this must be before function call
#     print("this is test function")


# Advanced
# functions with parameters
def printYourName(name):
    print('Your name is: ', name)


printYourName('YourName')
printYourName('Jack')

# function with multiple parameters


def add(a, b):
    print('Addition is: ', a+b)


add(4, 5)

# if you want this value in an variable, then result should be returned from the function


def addWithoutReturn(a, b):
    c = a+b  # c must be returned


print('4 + 5 = ', addWithoutReturn(4, 5))  # None

# function with return value


def addWithReturn(a, b):
    c = a+b
    return c


print('4 + 5 = ', addWithReturn(4, 5))  # 9

# functions with default parameters


def functionWithDefaultParams(a=4, b=5):
    return a+b


print("Values passed: ", functionWithDefaultParams(10, 10))  # 20
print("No values passed: ", functionWithDefaultParams())  # 4 + 5 = 9
