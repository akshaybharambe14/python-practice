# working with boolean operations and/or
# and - true if all conditions are true
# or  - true if any condition is true


def checkLoginAnd(username, password):
    if username == 'test' and password == '123':
        print('Valid user!')  # if both are correct
    else:
        print('Invalid user!')


checkLoginAnd('test', '123')  # valid
checkLoginAnd('test', '1231')  # wrong password


def checkLoginOr(username, password):
    if username == 'test' or password == '123':
        print('Valid user!')  # if any one is correct
    else:
        print('Invalid user!')


checkLoginOr('test', '1134')  # valid
checkLoginOr('test1', '1134')  # both are wrong
