names = ['a', 'b', 'c', 'd', 'e']
# len - get length of the list
print('length of names: ', len(names))

# append - add an element to the end
names.append('f')
print('names after append: ', names)

# # you can append another whole list, but will be added to single index
# newNames = ['x', 'y', 'z']
# names.append(newNames)
# print('names after append: ', names)
# insert
names.insert(1, 'aa')  # at existing postion
print('names after insert: ', names)

# at new postion, if greater than len + 1, it will be added to next continuous position
names.insert(15, 'aa')
print('names after insert: ', names)
print('length of names: ', len(names))

# index
print('Position of a: ', names.index('a'))  # single occurrence
# multiple occurrences, will give first possible
print('Position of aa: ', names.index('aa'))
# start will be 0 if not provided, and end will be max length
print('Position of aa: ', names.index('aa', 5))
