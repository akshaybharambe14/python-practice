# operations that can be performed on lists
numbers = [1, 2, 3, 4]
print('numbers: ', numbers)
# 1. change value of an element at position
numbers[1] = 7  # change 2 to 7
print('numbers after changing value: ', numbers)
# 2. add two lists
newNumbers = [8, 9, 10]
combinedList = numbers + newNumbers
print('numbers + newNumbers: ', combinedList)
# 3. duplicate list by multiplication, we can not multiply a list with another list
print('combinedList * 2: ', combinedList * 2)
# 4. check if given element is present in list
print(1 in combinedList)  # will print True because 1 is present
print(11 in combinedList)  # will print False because 11 is not present
