# checks for multiple conditions, if current condition is not satisfied, control will go to next condition

# Todo: Grading system

marks = int(input('Enter your marks: '))

# check for marks

if marks >= 90:
    print("A")
elif marks >= 60:
    print("B")
elif marks >= 30:
    print("c")
else:  # else is optional
    print("F")
