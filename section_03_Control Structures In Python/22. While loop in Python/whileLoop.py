# do someting till the condition is satisfied

# for loop with while loop
# we need counter as while loop does not maintain counter

a = list(range(0, 11))
for x in a:
    print('by for loop - ', x)

# same using while loop
counter = 0
while counter < len(a):
    print('by while loop - ', a[counter])
    counter += 1  # important, otherwise it will go in infinite loop i,e the program will never end

# break in while, exit from loop on condition
counter = 0
while counter < len(a):
    print('Using break: ', a[counter])
    if a[counter] == 5:
        print('Exiting loop')
        break
    counter += 1

# continue - skip operation
counter = 0
while counter < len(a):
    if a[counter] == 5:
        print('Skiping iteration for 5')
        counter += 1
        continue
    print('Using continue: ', a[counter])  # this will not print 5
    counter += 1
