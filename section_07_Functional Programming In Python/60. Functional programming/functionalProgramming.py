# functional programming - execute the program in sequence of functions
# here input to a function can be the result of a function which is further passed to another function


def add_ten(x):
    return x + 10


# now if we want to add 20, we need to execute this function twice
print("After adding 20 to 10: ", add_ten(add_ten(10)))

# this can be done in another way,
# instead of passing the return value, we can pass whole function as an argument


def twice(func, arg):  # execute any function twice with provided argument
    return func(func(arg))


print("After calling the function twice: ", twice(add_ten, 10))
