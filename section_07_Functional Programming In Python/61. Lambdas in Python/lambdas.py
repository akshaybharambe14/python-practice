# with the use of lambdas we can execute a functionality which can be enclosed in an function, anonymiously.
# here we don't need to define a function, instead, the specified expression is evaluated and directly returned

# ((lambda parameter_list: expression)(parameter))
print((lambda x: x**2)(4))
# with multiple parameters
print((lambda x, y: x**2 + y**2)(4, 4))
