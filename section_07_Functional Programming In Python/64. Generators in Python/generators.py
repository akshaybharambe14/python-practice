# generators are used to generate lists matching specified rule


def generate():
    count = 0
    while count < 5:
        yield count  # generate
        count += 1


print("Generated list: ", list((generate())))

for x in generate():
    print(x)

# generate a list of even numbers


def generate_even(x):
    for i in range(x):
        if i % 2 == 0:
            yield i


print("Even numbers: ", list(generate_even(101)))
