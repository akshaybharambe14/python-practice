# filters are used along with lambadas to filter out the given list

numbers = [1, 2, 4, 6, 7, 13, 54, 0, 4]
print("List: ", numbers)
result = list((filter(lambda x: x % 2 == 0, numbers)))
print("Only even numbers from list: ", result)

# we can replace the lambada by function


def checkEven(x):
    return x % 2 == 0


result = list(filter(checkEven, numbers))
print("Result by passing function: ", result)


# same thing can be done by list comprehension without filters

print("Result by list comprehension: ", [x for x in numbers if x % 2 == 0])
