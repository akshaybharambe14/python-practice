# map allows us to perform an operation on iterables like list

# add 2 to a number, for this we can define a function


def add_two(x):
    return x+2


# now what if we want to add 2 to each item in list
numbers = [1, 2, 3]
counter = 0
for x in numbers:
    numbers[counter] = add_two(x)
    counter += 1

print("Added 2 to each item in list: ", numbers)

# maps make this as easy as follows
# map(functionToBeCalled, iterable)
numbers = [1, 2, 3]
result = list(map(add_two, numbers))
print("Added 2 to each item using function with map: ", result)


# with the use of lambada functions

numbers = [1, 2, 3]
result = list(map(lambda x: x+3, numbers))
print("Added 3 to each item using lambdas with map: ", result)
