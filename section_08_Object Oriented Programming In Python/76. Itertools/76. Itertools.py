from itertools import count, accumulate, takewhile  # import module

# count generate numbers from given point to infinity, need to specify stopping criteria

print("Count - ")
for x in count(5):
    print(x)
    # stop if x is 20
    if x == 20:
        break

print("accumulate - ")  # adds all the previous numbers to current number
l = list(range(10))
print(l)
r = list(accumulate(l))
print(r)

print("takewhile - ")  # takes a function, if returns true then only add to list
print(list(takewhile(lambda x: x <= 10, r)))
