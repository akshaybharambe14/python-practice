# object oriented programming is the methodology to write program
# in which the attributes and methods are defined for a class
# and accessed through class object

# define class: class className:


class student():
    # initialise attributes for object of class student with provided name and contact,
    # where name and contact are attributes of the class
    def __init__(self, name, contact):
        self.name = name
        self.contact = contact

    # define some functions for the class called as methods
    def getStudentData(self):
        self.name = input("Enter student name: ")
        self.contact = input("Enter student contact: ")

    def printStudentData(self):
        print("Name: ", self.name, " Contact: ", self.contact)


# create an object of class student
john = student("None", 0)
# this will create object john with default attributes
john.printStudentData()
# accept new info
john.getStudentData()
john.printStudentData()

# create another object
rob = student("None", 0)
rob.getStudentData()
rob.printStudentData()
