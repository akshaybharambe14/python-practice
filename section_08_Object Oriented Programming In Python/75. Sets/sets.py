# set is an datastructure which contains only unique values

# define a set
a = {1, 2, 3, 4, 5, 6}
# check if an element is present or not
print("Is 2 present in set? ", 2 in a)

# add an item to set
a.add(7)
print("After adding 7: ", a)
# remove an item from set
a.remove(1)
print("After removing 1: ", a)

print("a: ", a)
b = {4, 5, 6, 7, 8, 9}
print("b: ", b)
# union of two sets - all unique items from both set
print("a union b: ", a | b)
# intersection - all common values
print("a intersection b: ", a | b)
# subtraction  a - b: all values from a which are not present in b
print("a - b", a - b)
