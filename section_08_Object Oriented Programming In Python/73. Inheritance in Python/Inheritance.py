# inherit parent proreties and methods to child

# Here student is a parent class


class student():
    # initialise attributes for object of class student with provided name and contact,
    # where name and contact are attributes of the class
    def __init__(self, name, contact):
        self.name = name
        self.contact = contact

    # define some functions for the class called as methods
    def getStudentData(self):
        self.name = input("Enter student name: ")
        self.contact = input("Enter student contact: ")

    def printStudentData(self):
        print("Name: ", self.name, " Contact: ", self.contact)


class sscStudent(student):
    # inherit sscStudent from student class
    def __init__(self, sscMarks, school):
        self.sscMarks = sscMarks
        self.school = school

    def getSSCDetails(self):
        self.sscMarks = input("Enter ssc marks: ")
        self.school = input("Enter school: ")

    def printSSCDetails(self):
        print("Marks: ", self.sscMarks,  " School: ", self.school)


# create object of sscStudent

ram = sscStudent(0, "None")
# methods from student class are also available for this object
ram.getStudentData()
ram.printStudentData()
ram.printSSCDetails()
ram.getSSCDetails()
ram.printSSCDetails()
