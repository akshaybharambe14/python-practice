# encapsulation or data hiding: make data accessible only in class


class student:
    __internalMarks = 5  # make this hidden outside class by adding __ at start

    def addMarks(self, marks):
        print("Final marks: ", marks + self.__internalMarks)

# create object


a = student()
a.addMarks(5)
# a.__internalMarks can not be accessed
