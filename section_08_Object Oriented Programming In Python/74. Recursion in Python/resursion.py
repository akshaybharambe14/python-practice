# recursion is nothing but the function calling itself till the specified condition becomes true

# factorial = 5! = 5 * 4! = 5 * 4 * 3! ...


def factorial(x):
    if x == 1:
        return 1
    else:
        return x * factorial(x-1)  # calling itself


print("5! = ", factorial(5))
