# overload the inbuilt functionality to add with provided logic

# define a class


class point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # overload +
    def __add__(self, other):
        x = self.x + other.x
        y = self.x + other.y
        return point(x, y)

    def __str__(self):
        return "({0},{1})".format(self.x, self.y)


# create objects
p1 = point(1, 2)
p2 = point(2, 3)
print(p1 + p2)
