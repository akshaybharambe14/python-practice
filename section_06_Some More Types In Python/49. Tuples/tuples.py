# tuples are immutable, once stored, we can not change values

# create new tuple
numbers = (1, 2, 3, 4)

print("Tuple: ", numbers)

# access tuple values
print("At 2 we have: ", numbers[2])

# if we try to change the values, error will be thrown
# numbers[0] = 3  # not possible
