# join items in a list with seperator
numbers = ["1", "2", "3", "4"]  # the list must contain strings only
newString = ",".join(numbers)
print("After joining: ", newString)
# with :
newString = ":".join(numbers)
print("After joining: ", newString)

# replace string
print("After replacing there with world: ",
      "Hello there".replace("there", "world"))
# with multiple occurrences, it replaces all
print("After replacing there with world: ",
      "Hello there, there".replace("there", "world"))
# with multiple occurrences, only replace one
print("After replacing there with world: ",
      "Hello there, there".replace("there", "world", 1))

newStr = "Hello There"
print("Is 'Hello there' starts with 'Hello'",
      newStr.startswith("Hello"))

newStr = "Hello There"
print("Is 'Hello there' ends with 'There'",
      newStr.endswith("There"))

print("UPPERCASE: ", newStr.upper())
print("lowerscase: ", newStr.lower())
