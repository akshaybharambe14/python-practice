# get certain set of values from list

# listName[(start, including, 0 if not given): (end, excluding, max length if not given), (interval)]

numbers = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

print("List: ", numbers)
# get list from 2-8
print("List from 2 - 8: ", numbers[2:8])
# get numbers before 5
print("numbers before 5: ", numbers[:5])
# get numbers after 5
print("numbers after 5: ", numbers[5:])
# if start and end both are not given
print("List : ", numbers[:])  # this will print whole list
# slice list with interval
# this will print all numbers at even position, start and end is not provided
print("List with interval of 2", numbers[::2])

# slicing a string
name = "Here is my name"
print("String after slicing", name[4:])
