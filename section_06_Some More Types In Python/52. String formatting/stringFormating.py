# numbers = [1,2,3]
newString = "Numbers:{0}{1}{2}".format(1, 2, 3)
print("String from numbers", newString)

# with , seperator
newString = "Numbers:{0},{1},{2}".format(1, 2, 3)
print("String from numbers", newString)
# with custom args
newString = "Numbers:{x}/{y}/{z}".format(x=1, y=2, z=3)
print("String from numbers", newString)
