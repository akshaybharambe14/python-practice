# find max value
print("Max: ", max(0, 3, 9, 2))
# find min value
print("Min: ", min(0, 3, 9, 2))
# find absolute value i,e value without considering sign
print("Abs: ", abs(-10))
