# dictionary - a data structure which stores data in key value pairs

people = {"john": 23, "rob": 32}
print("Dictionary: ", people)
# access dictionary values
print("john/'s: ", people["john"])

# dictionary can store any type of data
random = {"John": 23, 2: "two", "sampleList": [1, 2, 2]}
print("Random Dictionary: ", random)
print("John/'s age: ", random["John"])
print("2: ", random[2])
print("sampleList: ", random["sampleList"])
# print("This is not present: ", random["test"])  # this will throw error

# add new value
random["NewKey"] = "NewValue"
print("Random Dictionary after adding new value: ", random)

# edit value
random["NewKey"] = "RandomValue"
print("Random Dictionary after editing: ", random)
