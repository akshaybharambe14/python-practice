# create a list with some defined rules
# format listname = [rule list contition]

squares = [x**2 for x in range(5)]
print("Squares from 0 - 5:", squares)

# with condition
squaresOfEvenNumbers = [x**2 for x in range(10) if x % 2 == 0]
print("Squares of even from 0 - 10:", squaresOfEvenNumbers)
