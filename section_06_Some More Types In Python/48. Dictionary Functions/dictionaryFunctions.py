# create numbers dictionary
numbers = {1: "One", 2: "Two", 3: "Three"}

# check if value is present in dictionary
print("Is one present? ", 1 in numbers)
print("Is five present? ", 5 in numbers)

# Aceess values by keys
print(numbers.get(1))  # one is present, it will print One
print(numbers.get(5))  # five is not present, it will print Nome

# Give a message if key is not present
# this will print custom message
print(numbers.get(10, "This key is not present"))
