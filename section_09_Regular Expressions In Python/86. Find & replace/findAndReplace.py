import re

string = "Hello, my name is John\nI am John"
pattern = "John"

newString = re.sub(pattern, "Rob", string)
print(newString)
