import re


def matchPattern(pattern, string):  # string must start with pattern
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")

# ^s must start with s
# y$ must end with y


pattern = "^gr.y$"
string = "grey"
matchPattern(pattern, string)
string = "arey"
matchPattern(pattern, string)
string = "fhfhrey"
matchPattern(pattern, string)
string = "gray"
matchPattern(pattern, string)
string = "grek"
matchPattern(pattern, string)
