import re


def searchPattern(pattern, string):  # returns true if any single match is found
    if re.search(pattern, string):
        print("Found")
    else:
        print("Not found")


# required format MH12AB3456
pattern = r"MH[0-9][0-9][A-Z][A-Z][0-9][0-9][0-9][0-9]"
string = "MH12AB3456"
searchPattern(pattern, string)
string = "MH12AB34AB"
searchPattern(pattern, string)
