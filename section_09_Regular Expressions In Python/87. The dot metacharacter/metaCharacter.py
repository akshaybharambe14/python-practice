import re


def matchPattern(pattern, string):  # string must start with pattern
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")


pattern = r"a.d"  # any single character between a and d
string = "aid"
matchPattern(pattern, string)
string = "\n"
matchPattern(pattern, string)  # not applicable for \n
string = "aiid"
matchPattern(pattern, string)
pattern = r"a..d"
string = "aiid"
matchPattern(pattern, string)
