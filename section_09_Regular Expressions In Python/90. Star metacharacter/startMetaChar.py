import re
# * meta character allows multiple occurrences of previous character or group


def matchPattern(pattern, string):  # string must start with pattern
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")


pattern = r"egge*"  # this will allow 0 or multiple e at the end
string = "egg"
matchPattern(pattern, string)
string = "egge"
matchPattern(pattern, string)
string = "eggee"
matchPattern(pattern, string)

# for group

pattern = r"egg(bacon)*"  # this will allow 0 or multiple bacon at the end
string = "egg"
matchPattern(pattern, string)
string = "eggbacon"
matchPattern(pattern, string)
string = "eggbaconbaconbaconbaconbacon"
matchPattern(pattern, string)
string = "eggbaconbaconbaconbaconbacone"
matchPattern(pattern, string)
string = "eeggbaconbaconbaconbaconbacone"
matchPattern(pattern, string)
