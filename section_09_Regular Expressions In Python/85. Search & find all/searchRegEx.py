import re


def matchPattern(pattern, string):  # string must start with pattern
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")


def searchPattern(pattern, string):  # returns true if any single match is found
    if re.search(pattern, string):
        print("Found")
    else:
        print("Not found")


def findPattern(pattern, string):  # find all the occurrences
    return print(re.findall(pattern, string))


pattern = "egg"
string = "weggeggeggw"

matchPattern(pattern, string)
searchPattern(pattern, string)
findPattern(pattern, string)
