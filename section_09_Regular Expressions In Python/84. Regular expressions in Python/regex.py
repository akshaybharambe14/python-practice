import re  # contains functions for regular expressions
# regex are used to check if a string matches with defined pattern


# check if the string is according to pattern
def matchPattern(pattern, string):
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")


# define a pattern
pattern1 = r"egg"  # start with r to be treated as a raw string
str1 = "eggeggegg"
matchPattern(pattern1, str1)  # matched
str2 = "weggeggegg"
matchPattern(pattern1, str2)  # not matched
str3 = "eggeggeggw"
matchPattern(pattern1, str3)  # matched
str3 = "eggweggegg"
matchPattern(pattern1, str3)  # matched
