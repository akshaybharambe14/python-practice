import re


def matchPattern(pattern, string):  # string must start with pattern
    if re.match(pattern, string):
        print("Matched")
    else:
        print("Not matched")


pattern = r"a.*e"  # anything in a and e
string = "adfgdgfsgge"
matchPattern(pattern, string)

pattern = r"a(bacon)*e"  # multiple bacon in a and e
string = "adfgdgfsgge"
matchPattern(pattern, string)
string = "abacone"
matchPattern(pattern, string)
string = "abaconbacone"
matchPattern(pattern, string)
