file = open(
    'section_05_Exception Handling & File Handling In Python/read.txt', 'w')  # specify path for file and the mode in which you want to open the file
file.write('text')
file.close()  # need to close this file after performing operation
