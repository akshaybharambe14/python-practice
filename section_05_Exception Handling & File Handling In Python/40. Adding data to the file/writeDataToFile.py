newDemoFile = open(
    'section_05_Exception Handling & File Handling In Python/write.txt', 'w')  # open in write mode
# if file is not present it will create the file
# if file is opened in write mode, we can not read the file
newDemoFile.write('new text')  # replace existing data with given text
# this will be added to existing text because we have not closed the file yet.
newDemoFile.write('another text')
newDemoFile.close()  # close the file

# again open file and write some text
newDemoFile = open(
    'section_05_Exception Handling & File Handling In Python/write.txt', 'w')
newDemoFile.write('super text')
newDemoFile.close()  # close the file
