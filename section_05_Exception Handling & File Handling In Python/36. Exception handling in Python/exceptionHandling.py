def divide(a, b):
    try:
        print('Division is: ', a/b)  # try this code
    except ZeroDivisionError:  # check for possible exception
        print('There is an divideByZero exception')  # if exception occur


divide(4, 2)  # normal execution
divide(4, 0)  # with exception
