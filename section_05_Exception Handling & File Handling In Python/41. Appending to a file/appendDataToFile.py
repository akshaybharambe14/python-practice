# with write mode whole data in file is replaced by new data
# with append we can add data tho exitsing data

# 1. read data
print('Reading data from file')
fileData = open(
    'section_05_Exception Handling & File Handling In Python/append.txt', 'r')
fileContent = fileData.read()
print('Content from file: ', fileData)
fileData.close()

# 2. write data
print('Writing data to file')
fileData = open(
    'section_05_Exception Handling & File Handling In Python/append.txt', 'w')
fileData.write("Existing Text \n")
fileData.close()

# 3. read data
print('Reading data from file')
fileData = open(
    'section_05_Exception Handling & File Handling In Python/append.txt', 'r')
fileContent = fileData.read()
print('Content from file after writing: ', fileData)
fileData.close()

# 4. append data, ope file in append mode
print('Appending data from file')
fileData = open(
    'section_05_Exception Handling & File Handling In Python/append.txt', 'a')
fileData.write('Append this data')
fileData.close()

# 5. read data
print('Reading data from file')
fileData = open(
    'section_05_Exception Handling & File Handling In Python/append.txt', 'r')
fileContent = fileData.read()
print('Content from file after appending: ', fileData)
fileData.close()
