demoFile = open(
    'section_05_Exception Handling & File Handling In Python/read.txt', 'r')  # open in red mode
# in read mode, error will occur if file is not present at given location
fileContent = demoFile.read()  # read whole content
print('File content: ', fileContent)
demoFile.close()
# you need to close and reopen the file to perform another read operation
demoFile = open(
    'section_05_Exception Handling & File Handling In Python/read.txt', 'r')
newfileContent = demoFile.read(10)  # read only 10 bytes
print('Specific content:', newfileContent)
demoFile.close()

demoFile = open(
    'section_05_Exception Handling & File Handling In Python/read.txt', 'r')
newfileContent = demoFile.readline()  # read only single line
print('line content:', newfileContent)
demoFile.close()
