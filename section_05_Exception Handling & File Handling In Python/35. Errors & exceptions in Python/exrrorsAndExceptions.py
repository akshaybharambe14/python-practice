# errors can be syntax error or logical errors
# if syntax is not followed then syntax error is raised bby compiler


# def Test()  # syntax error as : is missing after function definition
#     print('Test function')

# logical errors are due to error in logic
# ex: if we want to add and we end up using * sign

def add(a, b):
    return a * b  # logical error, as we want to add


# Exceptions are raised when the logic is correct but program can not continue execution because of limitions
# this is logically correct but, the answer is infinity and infinity is not defined.
print((11 + 5) / (4-4))
# this will throw an exception and execution will be stopped

print('Done')  # this will not get executed
