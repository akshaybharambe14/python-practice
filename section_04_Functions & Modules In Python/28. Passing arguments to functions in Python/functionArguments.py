# functions with parameters
def printYourName(name):
    print('Your name is: ', name)


printYourName('YourName')
printYourName('Jack')

# function with multiple parameters


def add(a, b):
    print('Addition is: ', a+b)


add(4, 5)
add(9, 10)
