def add(a, b):
    return a+b


def square(c):
    return c * c


# square function accepts add function as an argument,
# so inner function add will be executed first and result will be passed to square function
result = square(add(2, 2))
print(result)  # 16
