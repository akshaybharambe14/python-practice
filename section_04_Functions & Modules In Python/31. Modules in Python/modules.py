import random  # name the module you want to use

# access function from imported module, moduleName.functionName
print(random.randint(0, 5))
print(random.randint(200, 500))
print(random.randint(20, 50))
# every time, when the code is executed, we will get different result
