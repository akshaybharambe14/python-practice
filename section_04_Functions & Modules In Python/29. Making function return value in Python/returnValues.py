# if you want result in an variable, then result should be returned from the function


def addWithoutReturn(a, b):
    c = a+b  # c must be returned


print('4 + 5 = ', addWithoutReturn(4, 5))  # None

# function with return value


def addWithReturn(a, b):
    c = a+b
    return c


print('4 + 5 = ', addWithReturn(4, 5))  # 9

# functions with default parameters


def functionWithDefaultParams(a=4, b=5):
    return a+b


print("Values passed: ", functionWithDefaultParams(10, 10))  # 20
print("No values passed: ", functionWithDefaultParams())  # 4 + 5 = 9
