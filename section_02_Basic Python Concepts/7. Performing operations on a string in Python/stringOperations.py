# concatenation

print('"a" + "a" =', "a" + "a")
# error, we can not multiply string with string
# print('"a" * "a" =', "a" * "a")

print('"ab_" * 2 = ', "ab_" * 10)
# error, we can not concatenate string with int
# print('"a" + 2 = ', "a" + 2)

print("a" + "b" + 'c' + "d" + "\n" + "e")  # multiple string concatenation
