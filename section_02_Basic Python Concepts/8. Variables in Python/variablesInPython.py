# variables store data to be modified

# create new variable of type string
a = "newstring_"
# print a
print("a = ", a)

# create another variable of type string
b = "another string"
print("b = ", b)

# store addition of a and b to c
c = a+b
# print c
print("c = a + b : ", c)

a = 2  # int
b = 3  # int
print(a+b)

# You can assign a string to an existing int

print('Current a is int: ', a)

# now assign a string to a

a = "2"
print('Current a is string: ', a)

# delete variable

del a
print("a after deleting = ", a)  # will throw error, because a is deleted
