print("Hello python world")

print('Hello again')  # with ''

print('hey "ABC"')  # combination

print("Hey 'abc'")  # combination

print("Hey"'test'" 'abc'")
print("Hey"' abc')
print("Hey"" abc")

# basic operations
print(2 + 2)  # addition of int
print(2 + 2.0)  # addition of float

print(1 - 3)
print(3 - 1.0)

print(2 * 3)
print(2 * 3.0)

print(4/2)  # result is float
print(4//2)  # result is int
print(3//2)
