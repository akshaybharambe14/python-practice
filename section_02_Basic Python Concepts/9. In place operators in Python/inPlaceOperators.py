# normal way to increment value
a = 10
# increment a by 5
a+5  # will not be 15
print('Value after adding 5 ', a)
# add and again store to same variable
a = a + 5  # now a will be 15
print('Value after adding 5 ', a)

# another way to do this is with inplace operators
b = 10
b += 5
print('Value of b after adding 5 ', b)

# can be done with int with all operators
c = 0
c += 20
print(c)  # 20
c -= 10
print(c)  # 10
c *= 10
print(c)  # 100
c /= 10
print(c)  # 10.0

# can be done with strings with + & * operators

d = ""
d += "one_"
print(d)  # one_
d *= 10
print(d)  # one_one_one_one_one_one_one_one_one_one_
